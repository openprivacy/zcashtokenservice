package main

import (
	"bufio"
	"fmt"
	"git.openprivacy.ca/openprivacy/zcashtokenservice"
	"github.com/gtank/ristretto255"
	"os"
)

func main() {

	// NOTE: If you want to run this prototype then you will need to edit all of these parameters to match
	// your own setup.
	clientZcashAddress := "zs1kfwveu7w2tl395lfql5rhrmewylk6fqp7386wjl3qc40rvxuak5gj450gafnztc62jdm6suy4ey"
	tokenServiceZcashAddress := "zs1252tvdk394cph9pg28hpy45atzwf67xpctuedhtksac6d4a654x77akr022cx943rup5z7zsw8r"
	publicKey := ristretto255.NewElement()
	publicKey.UnmarshalText([]byte("IFS9viFSlzaYOpuuPAoYc8aUk7NKlgrjbVml5qjKJzE="))

	ctx, uri := zcashtokenservice.RequestTokenURI(tokenServiceZcashAddress, publicKey, clientZcashAddress)

	fmt.Printf("Please use a zcash wallet to have these tokens signed: %v", uri)

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("\nPlease paste in the servers response: ")
	response, _ := reader.ReadString('\n')

	tokens, err := zcashtokenservice.ProcessResponse(response, ctx)

	if err == nil {
		fmt.Printf("\nSuccessfully signed tokens using Zcash!!:\n")
		for _, token := range tokens {
			fmt.Printf("Token: %x\n", token)
		}
		return
	}
	fmt.Printf("Error: %v", err)
}

package zcashtokenservice

import (
	"cwtch.im/tapir/primitives/privacypass"
	"encoding/base64"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"net/url"
	"testing"
)

func TestRequestTokenURI(t *testing.T) {

	log.SetLevel(log.LevelDebug)
	tokenServer := privacypass.NewTokenServer()

	ctx, uri := RequestTokenURI("zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f", tokenServer.Y, "zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f", nil)

	// Test that UI is Well Formed
	t.Logf("URI: %v", uri)
	url, err := url.Parse(uri)
	if err != nil {
		t.Fatalf("Cannot process uri %v", err)
	}

	// Check that Request & Response are well formed
	memo := url.Query().Get("memo")
	if len(memo) > 512 {
		t.Fatalf(" request memo is too long %v", len(memo))
	}
	memostr, _ := base64.StdEncoding.DecodeString(memo)
	t.Logf("request memo %s length is %v", memostr, len(memo))

	response, _ := ProcessRequest(memo, "zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f", tokenServer)

	responsestr, _ := base64.StdEncoding.DecodeString(response)
	if len(response) > 512 {
		t.Fatalf("response memo is too long %s %v", responsestr, len(response))
	}
	t.Logf("response memo length is %s %v", response, len(response))

	tokens, err := ProcessResponse(response, ctx)
	if err != nil {
		t.Fatalf("Tokens were not signed: %v", err)
	}

	// Test Tokens are Valid
	for _, token := range tokens {
		err = tokenServer.SpendToken(token.SpendToken([]byte("test")), []byte("test"))
		if err == nil {
			t.Logf("Spent token %v Successfully", token)
		} else {
			t.Errorf("Failed to spend token: %v", err)
		}
	}

	t.Logf("Testing with Constraint Token: %v", tokens[0])
	ctx, uri = RequestTokenURI("zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f", tokenServer.Y, "zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f", tokens[0])
	url, _ = url.Parse(uri)
	memo = url.Query().Get("memo")
	if len(memo) > 512 {
		t.Fatalf(" request memo is too long %v", len(memo))
	}
	response, _ = ProcessRequest(memo, "zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f", tokenServer)
	tokens, err = ProcessResponse(response, ctx)

	if err != nil {
		t.Errorf("Failed to unblind tokens: %v", err)
	}

	// Test Tokens are Valid
	for _, token := range tokens {
		err = tokenServer.SpendToken(token.SpendToken([]byte("test")), []byte("test"))
		if err == nil {
			t.Logf("Spent token %v Successfully with Constraint Token", token)
		} else {
			t.Logf("Failed to spend token")
		}
	}

}

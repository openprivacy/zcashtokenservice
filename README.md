# Zcash Token Service

This repository contains a *prototype* implementation of a token service 
(described in this [paper](./paper/)) and based on the privacypass implementation in Tapir.

`client/client.go` implements the client portion, generating a set of tokens and sending these 
to the zcash token service through an [encrypted memo](https://electriccoin.co/blog/encrypted-memo-field/) along with payment and a return address.

`server/server.go` implements the server portion, polling zcash for new transactions, signing the
tokens it receives along with a proof and sending both back through an encrypted memo to the specified
return address.
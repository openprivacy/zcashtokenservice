module git.openprivacy.ca/openprivacy/zcashtokenservice

go 1.13

require (
	cwtch.im/tapir v0.1.13
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.6
	git.openprivacy.ca/openprivacy/zcashrpc v0.0.0-20191201220044-80615a955ce3
	github.com/gtank/ristretto255 v0.1.1
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
)
